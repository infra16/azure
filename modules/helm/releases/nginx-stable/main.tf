resource "helm_release" "nginx-ingress" {
  name = "nginx-ingress-dev"
  chart = "nginx-ingress"
  repository = "https://helm.nginx.com/stable"
}
