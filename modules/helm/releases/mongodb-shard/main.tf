resource "helm_release" "mongodb-sharded" {
  name = "mongodb-sharded-dev"
  chart = "mongodb-sharded"
  repository = "https://charts.bitnami.com/bitnami"
  timeout = 900
}
