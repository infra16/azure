resource "helm_release" "postgres" {
  name = "postgresql-dev"
  chart = "postgresql"
  repository = "https://charts.bitnami.com/bitnami"
}
