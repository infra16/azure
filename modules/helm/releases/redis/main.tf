resource "helm_release" "redis" {
  name = "redis-dev"
  chart = "redis"
  repository = "https://charts.bitnami.com/bitnami"
}
