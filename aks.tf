resource "azurerm_resource_group" "rg" {
  name     = "aks-cluster"
  location = "east us"
}

resource "random_id" "log_analytics_workspace_name_suffix" {
  byte_length = 8
}

resource "azurerm_log_analytics_workspace" "log_analytics_workspace" {
  name                = "log-${azurerm_resource_group.rg.name}-analytics-workspace-${random_id.log_analytics_workspace_name_suffix.dec}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "PerGB2018"
}

resource "azurerm_log_analytics_solution" "azurerm_log_analytics_solution" {
  solution_name         = "ContainerInsights"
  location              = azurerm_log_analytics_workspace.log_analytics_workspace.location
  resource_group_name   = azurerm_resource_group.rg.name
  workspace_resource_id = azurerm_log_analytics_workspace.log_analytics_workspace.id
  workspace_name        = azurerm_log_analytics_workspace.log_analytics_workspace.name

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/ContainerInsights"
  }
}

resource "azurerm_virtual_network" "network" {
  name                = "aks-vnet"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.1.0.0/16"]
}

resource "azurerm_subnet" "subnet" {
  name                      = "aks-subnet"
  resource_group_name       = azurerm_resource_group.rg.name
  address_prefix            = "10.1.0.0/24"
  virtual_network_name      = azurerm_virtual_network.network.name
}

resource "azurerm_network_security_group" "sg" {
  name                = "aks-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "HTTPS"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTP"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_kubernetes_cluster" "cluster" {
  name       = "aks"
  location   = azurerm_resource_group.rg.location
  dns_prefix = "aks"
  resource_group_name = azurerm_resource_group.rg.name
  kubernetes_version  = "1.16.7"

  default_node_pool {
    name       = "default"
    node_count = "1"
    vm_size    = "Standard_B2ms"
    vnet_subnet_id = azurerm_subnet.subnet.id
  }

  service_principal {
    client_id     = var.ARM_CLIENT_ID
    client_secret = var.ARM_CLIENT_SECRET
  }

  addon_profile {

    kube_dashboard {
      enabled = true
    }

    oms_agent {
      enabled                    = true
      log_analytics_workspace_id = azurerm_log_analytics_workspace.log_analytics_workspace.id
    }
  }

  network_profile {
    network_plugin = "azure"
  }
}

output "env-dynamic-url" {
  value = azurerm_kubernetes_cluster.cluster.kube_config.0.host
}

output "kubeConfig" {
  value = azurerm_kubernetes_cluster.cluster.kube_config_raw
}
