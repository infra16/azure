terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    token    = "zn5or32lDAX4mA.atlasv1.gkRMxbUdwTzkY9rRE3MrLZ7i9mnAlFkl6fiC0yVht3hzcIxX7kmeUlvVoCHlYPez1MU"
    organization = "ald-rmsrapiddev"
    workspaces {
      name = "azure-aks"
    }
  }
}


provider "azurerm" {
   version = ">= 2.0"
   features {}
   subscription_id = var.ARM_SUB_ID
   client_id = var.ARM_CLIENT_ID
   client_secret = var.ARM_CLIENT_SECRET
   tenant_id = var.ARM_TENANT_ID
}

provider "helm" {
  version = "1.1.0"
  kubernetes {
    load_config_file = false
    
    host     = azurerm_kubernetes_cluster.cluster.kube_config.0.host
    client_key             = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.client_key)
    client_certificate     = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.client_certificate)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.cluster.kube_config.0.cluster_ca_certificate)
  }
}

module "helm-ingress" {
  source = "./modules/helm/releases/nginx-stable"
}

#module "helm-redis" {
#  source = "./modules/helm/releases/redis"
#}

#module "helm-mongodb-sharded" {
#  source = "./modules/helm/releases/mongodb-shard"
#}

#module "helm-postgres" {
#  source = "./modules/helm/releases/postgres"
#}
